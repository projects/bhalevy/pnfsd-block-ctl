CC= gcc
OBJS=	ctl.o efi.o
CFLAGS= -I../../linux-pnfs.working/include -Wall -Wextra -Werror

all: ctl

ctl: $(OBJS)
	$(CC) -o ctl $(OBJS) -levent -lpthread -lparted

clean:
	rm -f $(OBJS) ctl
